// _Experiment_Diff_Between_LH_RH_View_Matrices.cpp : Defines the entry point for the console application.
//

// @note. Background info is in Resource Files. (some math knowledge, etc...)

#include "stdafx.h"
#include <DirectXMath.h>
#include <iostream>
using namespace DirectX;
using namespace std;

// Overload the  "<<" operators so that we can use cout to 
// output XMVECTOR and XMMATRIX objects.
ostream& XM_CALLCONV operator << (ostream& os, FXMVECTOR v)
{
	XMFLOAT4 dest;
	XMStoreFloat4(&dest, v);

	os << "(" << dest.x << ", " << dest.y << ", " << dest.z << ", " << dest.w << ")";
	return os;
}

ostream& XM_CALLCONV operator << (ostream& os, FXMMATRIX m)
{
	for (int i = 0; i < 4; ++i)
	{
		os << XMVectorGetX(m.r[i]) << "\t";
		os << XMVectorGetY(m.r[i]) << "\t";
		os << XMVectorGetZ(m.r[i]) << "\t";
		os << XMVectorGetW(m.r[i]);
		os << endl;
	}
	return os;
}

int main()
{
	XMVECTOR mirrorPlane = XMVectorSet(0.0f, 1.0f, 1.0f, 0.0f); // xy plane
	XMMATRIX R = XMMatrixReflect(mirrorPlane);
	cout << R << endl;
	XMFLOAT3 data(2.0, 3.0, 4.0);
	XMVECTOR v = XMLoadFloat3(&data);
	cout << v << endl;
	cout << XMVector3Transform(v, R) << endl;
	cout << XMVector3TransformNormal(v, R) << endl;
}

