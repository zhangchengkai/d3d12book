// _Experiment_Diff_Between_LH_RH_View_Matrices.cpp : Defines the entry point for the console application.
//

// @note. Background info is in Resource Files. (some math knowledge, etc...)

#include "stdafx.h"
#include <DirectXMath.h>
#include <iostream>
using namespace DirectX;
using namespace std;

// Overload the  "<<" operators so that we can use cout to 
// output XMVECTOR and XMMATRIX objects.
ostream& XM_CALLCONV operator << (ostream& os, FXMVECTOR v)
{
	XMFLOAT4 dest;
	XMStoreFloat4(&dest, v);

	os << "(" << dest.x << ", " << dest.y << ", " << dest.z << ", " << dest.w << ")";
	return os;
}

ostream& XM_CALLCONV operator << (ostream& os, FXMMATRIX m)
{
	for (int i = 0; i < 4; ++i)
	{
		os << XMVectorGetX(m.r[i]) << "\t";
		os << XMVectorGetY(m.r[i]) << "\t";
		os << XMVectorGetZ(m.r[i]) << "\t";
		os << XMVectorGetW(m.r[i]);
		os << endl;
	}
	return os;
}

int main()
{
	XMVECTOR a = XMVectorSet(1.0f, 0.0f, 2.0f, 1.0f);
	XMVECTOR b = XMVectorSet(1.0f, 0.0f, 1.5f, 1.0f);
	XMVECTOR c = XMVectorSet(0.0f, 0.0f, 2.0f, 1.0f);

	cout << "In view space..." << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;

	XMMATRIX ProjL = XMMatrixPerspectiveFovLH(0.25f*3.1415926535f, (float)800 / 600, 1.0f, 1000.0f);

	cout << "LH proj matrix:" << endl;
	cout << ProjL << endl;

	a = XMVector4Transform(a, ProjL);
	b = XMVector4Transform(b, ProjL);
	c = XMVector4Transform(c, ProjL);
	
	cout << "In ProjL space (homogenerous clip space)..." << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;

	XMFLOAT4 a4, b4, c4;
	XMStoreFloat4(&a4, a);
	XMStoreFloat4(&b4, b);
	XMStoreFloat4(&c4, c);

	a = a / a4.w;
	b = b / b4.w;
	c = c / c4.w;

	cout << "In NDC space..." << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;

	XMMATRIX invProjL = XMMatrixInverse(&XMMatrixDeterminant(ProjL), ProjL);

	a = XMVector4Transform(a, invProjL);
	b = XMVector4Transform(b, invProjL);
	c = XMVector4Transform(c, invProjL);

	cout << "Then multiply invProj matrix..." << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;

	XMStoreFloat4(&a4, a);
	XMStoreFloat4(&b4, b);
	XMStoreFloat4(&c4, c);

	a = a / a4.w;
	b = b / b4.w;
	c = c / c4.w;

	cout << "Then divided by w...(we get view space coord again!)" << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;
}

