// _Experiment_Diff_Between_LH_RH_View_Matrices.cpp : Defines the entry point for the console application.
//

// @note. Background info is in Resource Files. (some math knowledge, etc...)

#include "stdafx.h"
#include <DirectXMath.h>
#include <iostream>
using namespace DirectX;
using namespace std;

// Overload the  "<<" operators so that we can use cout to 
// output XMVECTOR and XMMATRIX objects.
ostream& XM_CALLCONV operator << (ostream& os, FXMVECTOR v)
{
	XMFLOAT4 dest;
	XMStoreFloat4(&dest, v);

	os << "(" << dest.x << ", " << dest.y << ", " << dest.z << ", " << dest.w << ")";
	return os;
}

ostream& XM_CALLCONV operator << (ostream& os, FXMMATRIX m)
{
	for (int i = 0; i < 4; ++i)
	{
		os << XMVectorGetX(m.r[i]) << "\t";
		os << XMVectorGetY(m.r[i]) << "\t";
		os << XMVectorGetZ(m.r[i]) << "\t";
		os << XMVectorGetW(m.r[i]);
		os << endl;
	}
	return os;
}

int main()
{
	XMFLOAT3 mEyePos;
	// Convert Spherical to Cartesian coordinates.
	mEyePos.x = 0.0f;
	mEyePos.z = -5.0f;
	mEyePos.y = 0.0f;
	

	// Build the view matrix.
	XMVECTOR pos = XMVectorSet(mEyePos.x, mEyePos.y, mEyePos.z, 1.0f);
	XMVECTOR target = XMVectorZero();
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	cout << "eye position:" << endl;
	cout << pos << endl << endl;
	cout << "target:" << endl;
	cout << target << endl << endl;
	cout << "up direction:" << endl;
	cout << up << endl << endl;

	XMMATRIX viewL = XMMatrixLookAtLH(pos, target, up);
	cout << "LH view matrix:" << endl;
	cout << viewL << endl;
	XMMATRIX viewR = XMMatrixLookAtRH(pos, target, up);
	cout << "RH view matrix:" << endl;
	cout << viewR << endl;

	XMVECTOR a = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR b = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);
	XMVECTOR c = XMVectorSet(1.0f, 0.0f, 0.0f, 1.0f);

	cout << "In world space..." << endl;
	cout << "point A:" << endl;
	cout << a << endl << endl;
	cout << "point B:" << endl;
	cout << b << endl << endl;
	cout << "point C:" << endl;
	cout << c << endl << endl;

	cout << "In viewL space..." << endl;
	cout << "point A:" << endl;
	cout << XMVector4Transform(a, viewL) << endl << endl;
	cout << "point B:" << endl;
	cout << XMVector4Transform(b, viewL) << endl << endl;
	cout << "point C:" << endl;
	cout << XMVector4Transform(c, viewL) << endl << endl;

	cout << "In viewR space..." << endl;
	cout << "point A:" << endl;
	cout << XMVector4Transform(a, viewR) << endl << endl;
	cout << "point B:" << endl;
	cout << XMVector4Transform(b, viewR) << endl << endl;
	cout << "point C:" << endl;
	cout << XMVector4Transform(c, viewR) << endl << endl;

	XMMATRIX ProjL = XMMatrixPerspectiveFovLH(0.25f*3.1415926535f, (float)800 / 600, 1.0f, 1000.0f);
	XMMATRIX ProjR = XMMatrixPerspectiveFovRH(0.25f*3.1415926535f, (float)800 / 600, 1.0f, 1000.0f);

	cout << "LH proj matrix:" << endl;
	cout << ProjL << endl;
	cout << "RH proj matrix:" << endl;
	cout << ProjR << endl;

	cout << "In ProjL space..." << endl;
	cout << "point A:" << endl;
	cout << XMVector4Transform(XMVector4Transform(a, viewL), ProjL) << endl << endl;
	cout << "point B:" << endl;
	cout << XMVector4Transform(XMVector4Transform(b, viewL), ProjL) << endl << endl;
	cout << "point C:" << endl;
	cout << XMVector4Transform(XMVector4Transform(c, viewL), ProjL) << endl << endl;

	cout << "In ProjR space..." << endl;
	cout << "point A:" << endl;
	cout << XMVector4Transform(XMVector4Transform(a, viewR), ProjR) << endl << endl;
	cout << "point B:" << endl;
	cout << XMVector4Transform(XMVector4Transform(b, viewR), ProjR) << endl << endl;
	cout << "point C:" << endl;
	cout << XMVector4Transform(XMVector4Transform(c, viewR), ProjR) << endl << endl;

}

