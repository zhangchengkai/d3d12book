// _Test_GameTimer.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "../../Common/GameTimer.h"
using namespace std;
GameTimer t;

int main()
{
	t.Reset();
	t.LogInfo();
	t.LogInfo();

	t.Tick();
	t.LogInfo();
	t.LogInfo();

	t.Stop();
	t.LogInfo();
	t.LogInfo();

	t.Tick();
	t.LogInfo();
	t.LogInfo();

	t.Start();
	t.LogInfo();
	t.LogInfo();

	t.Tick();
	t.LogInfo();
	t.LogInfo();
	
	return 0;
}

